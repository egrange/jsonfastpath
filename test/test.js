﻿
var books = { 
	"books" : [
		{ "id"     : 1,  "title"  : "Clean Code", "author" : { "name" : "Robert C. Martin" }, "price"  : 17.96  },
		{ "id"     : 2, "title"  : "Maintainable JavaScript", "author" : { "name" : "Nicholas C. Zakas" }, "price"  : 10 },
		{ "id"     : 3, "title"  : "JavaScript: The Good Parts", "author" : { "name" : "Douglas Crockford" }, "price"  : 15.67 }
	], 
	"nums" :  {"1" : "one", "2" : "two", "3" : 3 },
	"leaf" :  [11, 22], 
	"profile-id" : 1234,
	"user_id" : null
};

function appendToResult(s) {
	document.querySelector('#results').insertAdjacentHTML('beforeend', s);
}

function check(o, q, r) {
	try {
		var t = JSONFastPath.apply(q, o);
	} catch (e) {
		appendToResult("<div>Error '"+e+"' for '"+q+"'</div>");
		return;
	}
	if (JSON.stringify(t)!=JSON.stringify(JSON.parse(r))) {
		appendToResult("<div>Failed '"+q+"', expected <ul><pre>"+r+"</pre> got <pre>"+JSON.stringify(t)+"</pre></ul></div>");
	}
}

check(books, '.books.author', '[{"name":"Robert C. Martin"},{"name":"Nicholas C. Zakas"},{"name":"Douglas Crockford"}]');
check(books, '.books.author.name', '["Robert C. Martin","Nicholas C. Zakas","Douglas Crockford"]');
check(books, '.books..name', '["Robert C. Martin","Nicholas C. Zakas","Douglas Crockford"]');
check(books, '.id', '[]');
check(books, '..id', '[1,2,3]');
check(books, '.*.id', '[1,2,3]');
check(books, '.books.1.price', '[10]');
check(books, '.books[1].price', '[10]');
check(books, '.books[-1]..name', '["Douglas Crockford"]');

check(books, '.books[*].price', '[17.96,10,15.67]');
check(books, '.leaf[*]', '[11,22]');

check(books, '.books[].id', '[1,2,3]');

check(books, '.books..title', '["Clean Code","Maintainable JavaScript","JavaScript: The Good Parts"]');

check(books, '..leaf', '[[11,22]]');
check(books, '..leaf[1]', '[22]');
check(books, '..leaf.."0"', '[11]');
check(books, '.leaf..nope', '[]');

check(books, '.books..*', '[1,"Clean Code","Robert C. Martin",17.96,2,"Maintainable JavaScript","Nicholas C. Zakas",10,3,"JavaScript: The Good Parts","Douglas Crockford",15.67]');
check(books, '.nums..*', '["one","two",3]');

check(books, '.nums[1]', '["one"]');
check(books, '.nums.2', '["two"]');
check(books, '.nums."3"', '[3]');

check(books, ' .books [ 2 ] .title ', '["JavaScript: The Good Parts"]');

check(books, '."profile-id"', '[1234]');
check(books, '."user_id"', '[null]');

var doubleQuotes = {
   'hello "world"' : 'foo',
   "test \"": 'bar'
};

check(doubleQuotes, '."hello ""world"""', '["foo"]');
check(doubleQuotes, '."test """', '["bar"]');

check([1,3,5], '[]', '[1,3,5]');
check([], '[]', '[]');

var objects = [
    { "ID" : "alpha" },
    { "ID" : "beta" }
];

check(objects, '.ID', '["alpha","beta"]');
check(objects, '..ID', '["alpha","beta"]');
check(objects, '."0".ID', '["alpha"]');
check(objects, '.*.ID','["alpha","beta"]');

appendToResult('<p>Tests completed.</p>');
