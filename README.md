# JSONFastPath #

Implements a minimalistic JSON querying syntax, simplified and speed-oriented.

## Documentation ##
-------------
JSONFastPath query expressions support only location paths.

###Location path###
To select items in JSONFastPath, you use a location path.

The location path consists of one or more location steps.

Every location step depend on the item you're trying to select:

* .property locates property immediately descended from context items
* ..property locates property deeply descended from context items
* [] locates an index item from context items (from the start when index is positive, from the end when index is negative)

You can use the wildcard symbol (*) instead of exact name of property:

* .* locates all properties immediately descended from the context items
* ..* locates all properties deeply descended from the context items
* [*] or [] locates all array items immediately descended from the context items
 
If you need to locate property containing non-alphanumerical characters, you can use quoted notation:

* ."property with non-alphanumerical characters"
* .."property with non-alphanumerical characters"
* ."property with ""escaped"" inner double quotes"

Consider the following JSON:
```javascript
var doc = {
    "books" : [
        {
            "id"     : 1,
            "title"  : "Clean Code",
            "author" : { "name" : "Robert C. Martin" },
            "price"  : 17.96
        },
        {
            "id"     : 2,
            "title"  : "Maintainable JavaScript",
            "author" : { "name" : "Nicholas C. Zakas" },
            "price"  : 10
        },
        {
            "id"     : 3,
            "title"  : "JavaScript: The Good Parts",
            "author" : { "name" : "Douglas Crockford" },
            "price"  : 15.67
        }
    ]
};
```

####Examples####
```javascript
// find all books authors
JSONFastPath.apply('.books.author', doc);
/* [{ name : 'Robert C. Martin' }, { name : 'Nicholas C. Zakas' }, { name : 'Robert C. Martin' }, { name : 'Douglas Crockford' }] */

// find all books author names
JSONFastPath.apply('.books.author.name', doc);
/* ['Robert C. Martin', 'Nicholas C. Zakas', 'Robert C. Martin', 'Douglas Crockford'] */

// find all names in books
JSONFastPath.apply('.books..name', doc);
/* ['Robert C. Martin', 'Nicholas C. Zakas', 'Robert C. Martin', 'Douglas Crockford'] */

// find price of last book in the list
JSONFastPath.apply('.books[-1].price', doc);
/* [15.67] */

```

###Compiled Queries###

Usually the .apply() method will be all you need, as it automatically maintains a cache of compiled queries. The cache is automatically purged periodically.

In cases where the cache could get polluted by dynamically generated queries, it is possible to bypass the cache and obtain a compiled query directly with the .compile() method.

####Examples####
```javascript
var myQuery = JSONFastPath.compile('..id');

myQuery.apply(doc);
/* [1,2,3] */
```