﻿/**
* JSONFastPath
*
* Copyright (c) 2015-2019 Eric Grange (eric@delphitools.info)
* Licensed under MPL 1.1
* https://www.mozilla.org/MPL/1.1/
*
* @version 1.3.3
*/

(function () {

function iterSkip(i) {
	while (i.p<i.n && i.s.charCodeAt(i.p)<=32) i.p++;
}
function iterGetProp(i) {
	var q = (i.s.charCodeAt(i.p) === 34) ? '"' : '', r = '', c = '';
	if (q) i.p++;
	do {
		c = i.s.charAt(i.p);
		if (c === q) {
			i.p++;
			if (i.s.charAt(i.p) === q) {
				r += q;
            i.p++;
			} else break;
		} else if (q || (c>='a' && c<='z') || (c>='A' && c<='Z') || (c>='0' && c<='9') || (c=='_')) {
			r += c;
			i.p++;
		} else break;
	} while (i.p<i.n);
	if (r === '') throw 'Invalid property';
	return r;
}
function iterGetIndex(i) {
	iterSkip(i);
	var c = i.s.charAt(i.p), r = '';
	if (c === '-') {
		r = c;
		i.p++;
		c = i.s.charAt(i.p)
	}
	while (c>='0' && c<='9') {
		r += c;
		i.p++;
		c = i.s.charAt(i.p)
	}
	if (r=='' || r=='-') throw 'Invalid index';
	return +r;
}
function immProp(v,r) {
	if (v === undefined || v === null) return;
	if (Array.isArray(v)) {
		var i = parseInt(this.p);
		if (isFinite(i)) {
			this.n.f(v[i], r)
		} else {
			for (i=0; i<v.length; i++) {
				this.f(v[i], r)
			}
		}
	} else {
		this.n.f(v[this.p], r)
	}
}
function indexProp(v,r) {
	if (v === undefined) return;
	if (this.p>=0) {
		this.n.f(v[this.p], r)
	} else {
		this.n.f(v[v.length+this.p], r)
	}
}
function forIn(v, r) {
	for (var k in v) {
		this.f(v[k], r)
	}
}
function deepProp(v,r) {
	if (v === undefined || v === null) return;
	if (Array.isArray(v)) {
		if (this.i !== undefined && v[this.i]!==undefined) {
			this.n.f(v[this.i], r);
			return
		}
		for (var i=0; i<v.length; i++) {
			this.f(v[i], r)
		}
	} else if (typeof v === 'object') {
		if (v[this.p] !== undefined) {
			this.n.f(v[this.p], r)
		} else {
			forIn.call(this, v, r)
		}
	} else if (this.a) this.n.f(v, r);
}
function allProp(v,r) {
	if (v === undefined || v === null) return;
	if (Array.isArray(v)) {
		for (var i=0; i<v.length; i++) this.n.f(v[i], r);
	} else {
		forIn.call(this.n, v, r)
	}
}
function select(v,r) {
	if (v !== undefined) r.push(v)
}
function apply(o) {
	var r = [];
	this.f(o, r);
	return r
}
function compileQuery(query) {
	var iter = { s: query, p: 0, n: query.length }, 
		c = 0,
		op = null, head = null, tail = null;
	while (iter.p<iter.n) {
		c = query.charCodeAt(iter.p);
		if (c<=32) { iter.p++; continue; }
		if (c===46) { // "."
			iter.p++;
			if (iter.p>=iter.n) throw 'Unterminated property selector';
			c = query.charCodeAt(iter.p);
			if (c===46) { // "."
				iter.p++;
				if (iter.p>=iter.n) throw 'Unterminated deep property selector';
                if (query.charCodeAt(iter.p) === 42) { // "*"
                    iter.p++;
                    op = { f: deepProp, a: 1 }
                } else {
				   op = { p: iterGetProp(iter), f: deepProp };
				   if (isFinite(parseInt(op.p))) {
					   op.i = parseInt(op.p)
				   }
                }
			} else if (c===42) { // "*"
				iter.p++;
				op = { f: allProp }
			} else {
				op = { p: iterGetProp(iter), f: immProp }
			}
		} else if (c===91) { // "["
			iter.p++;
			iterSkip(iter);
			c = query.charCodeAt(iter.p);
			if (c===42) { // "*"
				iter.p++;
				iterSkip(iter);
				op = { f: allProp }
			} else if (c==93) { // "]"
				op = { f: allProp }
			} else {
				op = { p: iterGetIndex(iter), f: indexProp }
			}
			iterSkip(iter);
			if (query.charCodeAt(iter.p)!=93) throw 'Missing "]"';
			iter.p++;
		} else {
			throw 'Unsupported character "'+query.charAt(iter.p)+'" ('+iter.p+') in query';
		}
		if (head) {
			tail.n = op
		} else {
			head = op;
		}
		tail = op;
	}
	if (!head) throw 'Empty query';
	tail.n = { f: select };
	head.apply = apply;
	return head;
}

var cache = {}, cacheN = 0;
function cachedQuery(q, o) {
	var c = cache[q];
	if (!c) {
		if (cacheN === 100) {
			cache = {};
			cacheN = 0;
		}
		cache[q] = c = compileQuery(q);
		cacheN++;
	}
	return c.apply(o);
}

JSONFastPath = { 
	version: "1.3.3",
	compile: compileQuery,
	apply: cachedQuery
}

})();
